import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-property-grid',
  templateUrl: './property-grid.component.html',
  styleUrls: ['./property-grid.component.css',
  '../../assets/vendor/animate.css/animate.min.css',
  '../../assets/vendor/bootstrap-icons/bootstrap-icons.css',
  '../../assets/vendor/bootstrap/css/bootstrap.min.css',
  '../../assets/css/style.css',
  '../../assets/vendor/swiper/swiper-bundle.min.css']
})
export class PropertyGridComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
