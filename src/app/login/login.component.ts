import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css',
  '../../assets/vendor/animate.css/animate.min.css',
  '../../assets/vendor/bootstrap-icons/bootstrap-icons.css',
  '../../assets/vendor/bootstrap/css/bootstrap.min.css',
  '../../assets/css/style.css',
  '../../assets/vendor/swiper/swiper-bundle.min.css']
})
export class LoginComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
