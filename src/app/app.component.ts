import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['../assets/vendor/animate.css/animate.min.css',
  '../assets/vendor/bootstrap-icons/bootstrap-icons.css',
  '../assets/vendor/bootstrap/css/bootstrap.min.css',
  '../assets/css/style.css',
  '../assets/vendor/swiper/swiper-bundle.min.css'
]
})
export class AppComponent {
  title = 'alten-immo';
}
